//
//  ConstantClass.swift
//  DuVerse
//
//  Created by P S 305 on 22/08/16.
//  Copyright © 2016 PS305. All rights reserved.
//

import UIKit
import Foundation


/// This class is mainly for all constants that are used in our app. To maintain the code optimization we are using this approach.


// For Live
let BaseUrl = "http://52.32.140.12/index.php/"

//For Testing and Debugging
//let BaseUrl = http://52.32.140.12/index.php/"

// MARK: Basic Constants Details:


let DEVICEID = UIDevice.current.identifierForVendor?.uuidString
let STORYBOARD = UIStoryboard(name: "Main", bundle: nil)
let OB_WEBSERVICE = WebServiceClass()//.sharedInstanceOfWebServiceClass
let SESSION = URLSession.shared   /// Its a singleton of session that creates a default session
let kAppDelegate = UIApplication.shared.delegate as! AppDelegate
let ApplicationVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"]
//let KeyboardHeight: CGFloat = 210.0
let KeyboardHeight: CGFloat = 260.0
let KeyboardHeight1: CGFloat = 160.0
let MaximumVideoDuration = 20.0000000000000


let ChatText =    "0"
let ChatImage =  "1"
let ChatVideo =   "2"
let ChatFolder =  "3"
let ChatUser =  "4"


func getDateFormat() -> String {
    let todaysDate = NSDate()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "YYYYMMdd_hhmmss"
    let DateInFormat = dateFormatter.string(from: todaysDate as Date)
    
    return DateInFormat
}

func getDateFormat1() -> String {
    let todaysDate = NSDate()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "YYYYMMddhhmmss"
    let DateInFormat = dateFormatter.string(from: todaysDate as Date)
    
    return DateInFormat
}

let IMAGENAME = "gourav\(getDateFormat()).png"
let VIDEONAME = "gourav\(getDateFormat()).mov"


/*
var DBSqliteSharedInstance: DBSqlite {
    //2
    struct Singleton {
        //3
        static let instance = DBSqlite()
    }
    //4
    return Singleton.instance
} */



struct ScreenSize {
    static let SCREEN_WIDTH         =   UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        =   UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    
    static let IS_PHONE = UIDevice.current.userInterfaceIdiom == .phone
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}

struct Version {
    static let SYS_VERSION = (UIDevice.current.systemVersion as NSString).floatValue
    static let iOS7 = (Version.SYS_VERSION < 8.0 && Version.SYS_VERSION >= 7.0)
    static let iOS8 = (Version.SYS_VERSION >= 8.0 && Version.SYS_VERSION < 9.0)
    static let iOS9 = (Version.SYS_VERSION >= 9.0 && Version.SYS_VERSION < 10.0)
}

struct ValidationMessages {
    static let strMoreThan5Files = "Only 5 media file can be upload at a time"
    static let strInternetIsNotAvailable = "Internet is not available, Please establish network connection"
    static let strServerNotResponding = "Server is not responding, Please try after sometime"
    static let strFolderLimitIsFull = "You cannot upload file in this folder, folder is full"
    
    // MARK: Login Msgs
    
    static let strEnterEmailOrPhone = "Please enter email or phone"
    static let strEnterValidEmail = "Please enter valid email"
    static let strEnterValidPhone = "Please enter valid 10 digit phone no"
    static let strEnterPassword = "Please enter password"
    static let strPasswordLength = "Password should be of 6 characters"
    
}

// MARK: Webservices Constants

struct WebServiceName {
    static let Login = "User/userLogin"
    static let Registration = "User/registration"
    static let UploadImage = "User/uploadImage"
    static let ForgotPassword = "Settings/forgotPassword"
    static let CreateFolder = "User/createFolder"
    static let MyFolderList = "User/myFolderList"
    static let Add_RemoveFavorite = "User/addFavourite"
    static let UploadProfileImage = "User/uploadProfileImg"
    static let Follow_Unfollow = "User/following"
    static let SendDuvRequest = "User/sendFriendReq"
    static let ResponseFriendRequest = "User/responseFriendReq"
    static let MyFriendList = "User/myFriendList"
    static let DeleteFolder = "User/deleteFolder"
    static let SetFollowerPrivacy = "User/setFollowerPrivacy"
    static let SetProfilePrivacy = "User/setProfilePrivacy"
    static let SendMessages = "Chat/sendMsg"
    static let ReceivedMessages = "Chat/chatAllMsg"
    static let QueAndComment = "Settings/queAndComment"
    static let SendImgComment = "Comment/imgComment"
    static let DeleteImgComment = "Comment/imgCommentDelete"
    static let EditImgComment = "Comment/imgCommentEdit"

    static let ReportUser = "Comment/reportUser"
    static let ChangePassword = "User/changePassword"
    static let UpdateProfileInfo = "User/updateProfileInfo"
    static let AllUserList = "Comment/allUserList"
    static let SendRequest = "User/sendFriendReq"
    static let Search = "Comment/search"
    static let DeleteFolderImage = "User/deleteImage"
    
    static let SetNotification = "Comment/setNotification"
    static let SetMuteUnmute = "Comment/setMute"
    static let MyFavouriteList = "Comment/myFavouriteList"
    static let Logout = "Comment/logout"
    static let AllFolderId = "Comment/getAllFolder"
    static let FolderInfo = "Comment/folderInfo"
    static let ChatLastMsg = "Chat/chatLastMsg"
    static let ChatLastMsgOfUser = "Chat/allChatLastMsg"
    static let GetUserInfo = "User/userInfo"
    
}

//MARK: Notifications Constants
struct NotificationTypes {
    static let ReceivedMessage = "3"
    static let FriendRequest = "0"
    static let Favorite = "1"
    static let FollowUser = "2"
}


//MARK: DataBase Constants:

class DataBaseConstants {
    static let ChatDataBaseName = kAppDelegate.dictUserData["login_id"] as! String
    static let TableUserChat = "UserChat"
    static let TableUserActivity = "UserActivity"
    
    static let AppVersion = "AppVersion"
    
    static let kSTextByme =  "textByme"
    static let kSTextByOther =  "textbyother"
    static let kSImagebyme  =  "ImageByme"
    static let kSImagebyOther =  "ImageByother"
    
    static let kStypeImage  =  "Image"
    static let kStypeText  = "Text"
    
    static let kSending  =  "Sending"
    static let kSent         =      "Sent"
    static let kFailed      =       "Failed"
    
    static let SoundName = "msg_tone"
    
    static let Sent = "sent"
    static let Received = "received"
    
    static let MsgId = "Msg_Id"
    static let ChatWithUserId = "ChatWithUserId"
    static let MessageType = "Message_Type"  /// Type of message like: Text, Image, any File etc.
    static let MessageStatus = "Message_Status" /// Send, Received etc.
    static let MessageStatus_Sent = "sent"
    static let MessageStatus_Received = "received"
    static let Message  = "Message"
    static let MessageTime = "Message_Time"
    static let MediaType = "Media_Type"
    static let ImageMsg = "Image"
    static let FilePath = "FilePath"
    static let UserImagePath = "UserImagePath"
    static let FileData = "FileData"
    static let MessageSendStatus = "Message_SendStatus"
    
    
    static let MessageType_Text = "Text"
    static let MessageType_Image = "Image"
    static let MessageType_Video = "Video"
    static let MessageType_File = "File"
    static let MessageType_Folder = "Folder"
    static let MessageType_User = "User"
    
    static let MessageDownloadStatus = "Message_downloadStatus"
    static let UnreadStatus = "UnreadStatus"
    static let MessageDate = "Message_Date"
    static let ChatDateFormat = "dd MMM yyyy hh:mm a"
    static let ChatDateFormatAlter = "YYYY-MM-dd hh:mm:ss"
    
    static let ChatTimeFormat = "hh:mm a"
    
    
    static let ActivityName = "ActivityName"
    static let ActivityData = "ActivityData"
    static let ActivityMessage = "ActivityMessage"
    static let ActivityType = "ActivityType"
    static let ActivityDate = "ActivityDate"
    
    static let ActivityType_SendRequest = "SendRequest"
    static let ActivityType_AcceptRequest = "AcceptRequest"
    static let ActivityType_FavoriteImage = "FavoriteImage"
    static let ActivityType_FavoriteVideo = "FavoriteVideo"
    static let ActivityType_FavoriteFolder = "FavoriteFolder"
    static let ActivityType_FavoriteUser = "FavoriteUser"
    static let ActivityUserId = "UserId"
    
    static let YouHaveANewMessage = "You have a new message from"
    static let WorkInProgressMsg = "The work is in progress"
    static let InternetNotAvailableMsg = "Internet connection is not available, Please try again later."
    static let DropBoxLogoutMsg = "Are you sure you want to logout from your account?"
    static let ServerNotRespondingMsg = "Server is not responding, please try again later"
    
}


// MARK: Other Supporting Functions:

struct Singletons {
    static let sharedInstanceOfNSMutableArray = NSMutableArray()
    static let sharedInstanceOfNSMutableDictionary = NSMutableDictionary()
}

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension UIImage {
    var uncompressedPNGData: NSData      { return UIImagePNGRepresentation(self)! as NSData        }
    var highestQualityJPEGNSData: NSData { return UIImageJPEGRepresentation(self, 1.0)! as NSData }
    var highQualityJPEGNSData: NSData    { return UIImageJPEGRepresentation(self, 0.75)! as NSData }
    var mediumQualityJPEGNSData: NSData  { return UIImageJPEGRepresentation(self, 0.5)! as NSData }
    var lowQualityJPEGNSData: NSData     { return UIImageJPEGRepresentation(self, 0.25)! as NSData }
    var lowestQualityJPEGNSData:NSData   { return UIImageJPEGRepresentation(self, 0.0)! as NSData }
}
/*
extension UITableView {
    func lastIndexpath() -> NSIndexPath {
        let section = self.numberOfSections > 0 ? self.numberOfSections - 1 : 0
        let row = self.numberOfRows(inSection: section) > 0 ? self.numberOfRows(inSection: section) - 1 : 0
        let indexPath = NSIndexPath(item:row, )//NSIndexPath(forItem: row, inSection: section)
        //print("indexPath = \(indexPath.row)")
        return indexPath
    }
} */

//extension String {
    func stringWithEncodingEmojisData(strWithEmoji: String) -> String {
        let data = strWithEmoji.data(using: String.Encoding.nonLossyASCII)!
        let strEncoded = String(data: data, encoding: String.Encoding.utf8)
        //print("strEncoded = \(strEncoded)")
        return strEncoded!
    }
    
    func stringWithDecodingEmojisData(strWithEncodedForm: String) -> String {
        /// For displaying Emojis
        var strReturn = "Untitled"
        _ = strWithEncodedForm
        if(strWithEncodedForm.characters.count > 0) {
            let jsonString = strWithEncodedForm.cString(using: String.Encoding.utf8)
            //print("jsonString = \(jsonString)")
            let jsonData = NSData(bytes: jsonString!, length: jsonString!.count)
            //print("jsonData = \(jsonData.length)")
            strReturn =  String(data: jsonData as Data, encoding: String.Encoding.nonLossyASCII)!
        }
         return strReturn
    }

func getTimeDifferenceBetweenTwoDates(strMsgDate: String) -> String  {
    
    print("strMsgDate =\(strMsgDate)")
    
    let strTimeMessage = ""
    /*
    if(strMsgDate.characters.count > 0){
        let df1 = DateFormatter()
        df1.dateFormat = "dd-MM-YYYY"
        let dateMsg = df1.date(from: strMsgDate)
        
        
        let todaysDate = NSDate()
        
        let df = DateFormatter()
        //[df setDateFormat:@"dd-MM-YYYY HH:mm:ss"];
        df.dateFormat = "yyyy-MM-dd HH:mm:ssZZZZZ"//DataBaseConstants.ChatDateFormatAlter
        let strdate = df.string(from: todaysDate as Date)
        
        
        print("strMsgDate = \(strMsgDate)")
        print("strdate = \(strdate)")
        
        var date1 = df.date(from: strMsgDate)
         print("date1 = \(date1)")
        if(date1 == nil) {
            df.dateFormat = "yyyy-MM-dd HH:mm:ssZZZZZ"
            date1 = df.date(from: strMsgDate)
        }
        
        let date2 = df.date(from: strdate)
        
        print("date1 = \(date1)")
        print("date2 = \(date2)")
        
        let interval = date2!.timeIntervalSinceDate(date1!)
        
        print("interval = \(interval)")
        
        let hours = Int(interval) / 3600
        print("hours = \(hours)")
        // integer division to get the hours part
        let minutes = (Int(interval) - (hours * 3600)) / 60
        print("minutes = \(minutes)")
        // interval minus hours part (in seconds) divided by 60 yields minutes
        let seconds = (Int(interval)  - (hours * 3600) - (minutes * 60))
        print("seconds = \(seconds)")
        // interval minus hours part (in seconds) divided by 60 yields minutes
        var timeDiff = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        
        if(seconds == 0 && seconds < 10 && hours == 0 && minutes == 0) {
            strTimeMessage = "Just now"
        } else if (seconds >= 10 && seconds < 59 && hours == 0 && minutes == 0) {
            strTimeMessage = String(seconds) + " seconds ago"
        } else if (minutes > 0 && minutes <= 59 && hours == 0) {
            if(minutes > 1) {
                strTimeMessage = String(minutes) + " minutes ago"
            } else {
                strTimeMessage = String(minutes) + " minute ago"
            }
            
        } else if (hours >= 1 && hours < 24) {
            if(hours > 1) {
                strTimeMessage = String(hours) + " hours ago"
            } else {
                strTimeMessage = String(hours) + " hour ago"
            }
            
        } else if (hours > 24) {
            strTimeMessage = String(describing: dateMsg)
        }
        
    } */
    return strTimeMessage
}






 /*func getDaysBetweenCreatedDate(_ createDate: String, andTodayDate todayDate: String) -> String {
    var timeLeft = "0"
    var dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var create = dateFormatter.dateFromString(createDate)
    var todate = dateFormatter.dateFromString(todayDate)!
    var seconds = create?.timeIntervalSinceDate(todate)
    var days = Int(floor(seconds! / (3600 * 24)))
    if days != 0 {
        Int(seconds!) -= days * 3600 * 24
    }
    var hours = Int(floor(seconds / 3600))
    if hours != 0 {
        seconds -= hours * 3600
    }
    var minutes = Int(floor(seconds / 60))
    if minutes != 0 {
        seconds -= minutes * 60
    }
    if days != 0 {
        timeLeft = "\(Int(days) * -1)"
    }
    else if hours != 0 {
        //  timeLeft = [NSString stringWithFormat: @"%ld H", (long)hours*-1];
    }
    else if minutes != 0 {
        //timeLeft = [NSString stringWithFormat: @"%ld M", (long)minutes*-1];
    }
    else if seconds != 0 {
        // timeLeft = [NSString stringWithFormat: @"%lds", (long)seconds*-1];
    }
    
    return timeLeft
} */

//}

/*folder_id :-
 [{"folder_delete_id":2},{"folder_delete_id":3}]
 http://52.32.140.12/index.php/User/deleteFolder*/
