//
//  Validation.h
//  ShopyFy
//
//  Created by P S 305 on 02/07/16.
//  Copyright © 2016 PS305. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Validation : NSObject

+(BOOL)EmailCheck:(NSString*)sender;
+(BOOL)PasswordCheck:(NSString*)sender;
+(BOOL)ContactCheck:(NSString*)sender;
+(BOOL)Checkname:(NSString*)sender;
+(BOOL)CheckLength:(NSString*)sender;
+(BOOL)CheckContactLength:(NSString*)sender;
+(BOOL)PasswordLenghtCheck:(NSString*)sender;
@end
