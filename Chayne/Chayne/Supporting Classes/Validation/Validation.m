//
//  Validation.m
//  ShopyFy
//
//  Created by P S 305 on 02/07/16.
//  Copyright © 2016 PS305. All rights reserved.
//

#import "Validation.h"

@implementation Validation

#pragma mark - Email Validation

+(BOOL)EmailCheck:(NSString*)sender
{
    NSString *emailRegex = @"[a-zA-Z0-9._-]+@[a-z_-]+\\.+[a-z]+";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (![emailValidation evaluateWithObject:sender]) {
        return NO;
    }
    return YES;
}

+(BOOL)PasswordCheck:(NSString*)sender
{
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789_.@#*&"] invertedSet];
    
    if ([sender rangeOfCharacterFromSet:set].location != NSNotFound) {
        NSLog(@"This string contains illegal characters");
        return NO;
    }
    return YES;
}

+(BOOL)ContactCheck:(NSString*)sender
{
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    
    if ([sender rangeOfCharacterFromSet:set].location != NSNotFound) {
        NSLog(@"This string contains illegal characters");
        return NO;
    }
    return YES;
}
#pragma mark - name validation

+(BOOL)Checkname:(NSString*)sender
{
    NSCharacterSet * set = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ "] invertedSet];
    
    if ([sender rangeOfCharacterFromSet:set].location != NSNotFound) {
        NSLog(@"This string contains illegal characters");
        return NO;
    }
    return YES;
}

#pragma mark - lenght validation

+(BOOL)CheckLength:(NSString*)sender
{
    if (sender.length == 0) {
        return NO;
    }
    return YES;
}

#pragma mark - contact validation

+(BOOL)CheckContactLength:(NSString*)sender
{
    if (sender.length < 10 && sender.length > 13) {
        return NO;
    }
    return YES;
}

#pragma mark - password validation

+(BOOL)PasswordLenghtCheck:(NSString*)sender
{
    if(sender.length < 3){
        return NO;
    }
    return YES;
}

@end




