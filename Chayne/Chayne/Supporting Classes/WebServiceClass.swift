//
//  WebServiceClass.swift
//  DuVerse
//
//  Created by P S 305 on 22/08/16.
//  Copyright © 2016 PS305. All rights reserved.
//

import UIKit

class WebServiceClass: NSObject {

    
    static let sharedInstanceOfWebServiceClass = WebServiceClass()
    
    
    func deallocateMemory()  {
        
    }
    
   // deinit {
   //  //   //print("Webservice class deallocated successfully")
   
    
    
/// this function takes webservice Name and arrary of params and return a genrated request
    
    func getWebServiceDataWithWebServiceName(webServiceName: String, arrParams: NSMutableArray, methodType: Int)  -> NSMutableURLRequest {
        //print("arrParams =\(arrParams)")
        
        let strUrl = NSString(format: "%@%@", BaseUrl, webServiceName)
        //print("strUrl = \(strUrl)")
        let url = NSURL(string: strUrl as String)!
      
        let request = NSMutableURLRequest(url: url as URL)
        autoreleasepool { 
            
        
        
        let boundary = "78876565564454554547676"
        
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        var strMethodType: String = ""
        if methodType == 1 {
            strMethodType = "POST"
        } else if methodType == 2 {
            strMethodType = "GET"
        } else if methodType == 3 {
            strMethodType = "PUT"
        }
        
        request.httpMethod = strMethodType // POST OR PUT What you want
        
        let body = NSMutableData()
        if(arrParams.count > 0) {
        let dictData = arrParams.object(at: 0) as!  NSMutableDictionary
        
        /// parsing or taking values and keys fro the array
        let arrKeys = dictData.allKeys as NSArray
        let arrValues = dictData.allValues as NSArray
        /// runs loop upto array count
        for i in 0 ..< arrKeys.count  {
            
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            // Append your parameters
            body.append("Content-Disposition: form-data; name=\"\(arrKeys.object(at: i))\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(NSString(format: "\(arrValues.object(at: i))\r\n" as NSString).data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!)
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
        }
        }
        request.httpBody = body as Data
            
            /*
            let delayTime = DispatchTime.now(DispatchTime.now(), Int64(5.0 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, DispatchQueue.main) {
            } */
            
        }
        //request.cachePolicy = NSURLRequest.CachePolicy.ty
        return request
    }
    
    func  sendImageDataWithWebServiceName(webServiceName:String, dictParams: [String: String]?, arrImages: NSArray)  -> NSMutableURLRequest {
        
        // var dictParams = dictParams
        let strUrl = NSString(format: "%@%@", BaseUrl, webServiceName)
        //print("strUrl = \(strUrl)")
        let url = NSURL(string: strUrl as String)!
        
        let request = NSMutableURLRequest(url: url as URL)
        let boundary = "Boundary-\(NSUUID().uuidString)"
        
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData();
        
        //print("dictParams = \(dictParams)")
        
        if dictParams != nil {
            for (key, value) in dictParams! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        } 
        
        // let strprint = String(data: body as Data, encoding: String.Encoding.utf8)
        //print("strprintBody First -= \(strprint)");
         let mimetype = "application/octet-stream"
        
        /// Now Image array logic
        let dictImage = arrImages[0] as! NSDictionary
        let fileData = dictImage["fileData"] as! NSData
        //print("Image fileData = \(fileData.length)")
        
        /// FileName
        if(fileData.length > 0) {
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"chat_image\"; filename=\"userImage.png\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(fileData as Data)
            body.appendString(string: "\r\n")
        }
        body.appendString(string: "--\(boundary)--\r\n")
        // let strprintFileName = String(data: body as Data, encoding: String.Encoding.utf8)
        //print("strprintFileName Array = \(strprintFileName?.characters.count)");
        
         //print("strprintBody -= \(body.length)");
         request.httpBody = body as Data
        
        /*let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
        } */
        
        return request
    }
    
    func  sendFolderDataWithWebServiceName(webServiceName:String, dictParams: [String: String]?,  arrImages: NSMutableArray)  -> NSMutableURLRequest{
        let strUrl = NSString(format: "%@%@", BaseUrl, webServiceName)
        //print("strUrl = \(strUrl)")
        let url = NSURL(string: strUrl as String)!
        
        let request = NSMutableURLRequest(url: url as URL)
        let boundary = "Boundary-\(NSUUID().uuidString)"
        
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData();
        
        //print("dictParams = \(dictParams)")
        var mimetype = "application/octet-stream"
        if dictParams != nil {
            for (key, value) in dictParams! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        for i in 0..<arrImages.count {
            // let dictImage = (arrImages[i] as AnyObject).mutableCopy() //as! NSDictionary
            let dictImage = arrImages[i] as! NSDictionary
            //print("dict = \(dictImage)")
            let strFileUrl = dictImage["file_name"] as! String
            let fileUrl = NSURL(string: strFileUrl)
            let fileData = NSData(contentsOf: fileUrl! as URL)
            
            
            let type = dictImage["file_type"] as! String
            let strThumbUrl = dictImage["thumbnail"] as! String
            var thumbUrl = NSURL()
            var thumbData = NSData()
            
            
            if(type == "0") {
                mimetype = "application/octet-stream"
            } else {
                mimetype = "video/mov"
                thumbUrl = NSURL(string: strThumbUrl)!
                thumbData = NSData(contentsOf: thumbUrl as URL)!
            }
            if((fileData?.length)! > 0) {
                
                
                body.appendString(string: "--\(boundary)\r\n")
                if(type == "0") {
                    body.appendString(string: "Content-Disposition: form-data; name=\"file_name\(i)\"; filename=\"\(IMAGENAME)\"\r\n")
                } else {
                    body.appendString(string: "Content-Disposition: form-data; name=\"file_name\(i)\"; filename=\"\(VIDEONAME)\"\r\n")
                }
                
                body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                body.append(fileData! as Data)
                body.appendString(string: "\r\n")
            }
            if(type == "1"){
                if(thumbData.length > 0) {
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"thumbnail\(i)\"; filename=\"thumbImage\(i).png\"\r\n")
                    body.appendString(string: "Content-Type: application/octet-stream\r\n\r\n")
                    body.append(thumbData as Data)
                    body.appendString(string: "\r\n")
                }
            }
            
            
        }
        body.appendString(string: "--\(boundary)--\r\n")
        
        
        //let strprintFileName = String(data: body as Data, encoding: String.Encoding.utf8)
        //print("strprintFileName Array = \(strprintFileName?.characters.count)");
        
        //print("strprintBody -= \(body.length)");
        request.httpBody = body as Data
//        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5.0 * Double(NSEC_PER_SEC)))
//        dispatch_after(delayTime, dispatch_get_main_queue()) {
//            
//            //  dictParams = nil
//            // arrImages.removeAllObjects()
//        }
        return request
    }
    
    func  sendVideoDataWithWebServiceName(webServiceName:String,
                                          dictParams: [String: String]?, arrImages: NSMutableArray)  -> NSMutableURLRequest {
        
        let strUrl = NSString(format: "%@%@", BaseUrl, webServiceName)
        //print("strUrl = \(strUrl)")
        let url = NSURL(string: strUrl as String)!
        
        let request = NSMutableURLRequest(url: url as URL)
        let boundary = "Boundary-\(NSUUID().uuidString)"
        
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData();
        
        //print("dictParams = \(dictParams)")
        
        if dictParams != nil {
            for (key, value) in dictParams! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        //  let strprint = String(data: body as Data, encoding: String.Encoding.utf8)
        //print("strprintBody First -= \(strprint)");
        let mimetype = "video/mov"
        
        /// Now Image array logic
        let dictImage = arrImages[0] as! NSDictionary
        let fileData = dictImage["fileData"] as! NSData
        let thumbData = dictImage["thumbData"] as! NSData
        //print("Image fileData = \(fileData.length), thumbData = \(thumbData.length)")
        
        /// FileName
        if(fileData.length > 0) {
            body.appendString(string: "--\(boundary)\r\n")
            body.appendString(string: "Content-Disposition: form-data; name=\"chat_image\"; filename=\"uploadedVideo.mov\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(fileData as Data)
            body.appendString(string: "\r\n")
          
            if(thumbData.length > 0) {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"thumbnail\"; filename=\"thumbImage.png\"\r\n")
                body.appendString(string: "Content-Type: application/octet-stream\r\n\r\n")
                body.append(thumbData as Data)
                body.appendString(string: "\r\n")
            }
            
        }
        
        /// ThumbNail
       /* if(fileData.length > 0) {
            body.appendString("--\(boundary)\r\n")
            body.appendString("Content-Disposition: form-data; name=\"thumbnail\"; filename=\"thumb1.png\"\r\n")
            body.appendString("Content-Type: \(mimetype)\r\n\r\n")
            body.appendData(thumbData)
            body.appendString("\r\n")
        } */
        
        body.appendString(string: "--\(boundary)--\r\n")
        //let strprintFileName = String(data: body as Data, encoding: String.Encoding.utf8)
        //print("strprintFileName Array = \(strprintFileName?.characters.count)");
        
        //print("strprintBody -= \(body.length)");
        request.httpBody = body as Data
        /*
        let delayTime = DispatchTime.now(dispatch_time_t(DISPATCH_TIME_NOW), Int64(5.0 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            
            dictParams = nil
            arrImages.removeAllObjects()
        } */
        
        
        return request
    }
    
    func  sendMultipleImageDataWithWebServiceName(webServiceName:String, dictParams: [String: String]?, arrImages: NSArray)  -> NSMutableURLRequest {
        
        let strUrl = NSString(format: "%@%@", BaseUrl, webServiceName)
        //print("strUrl = \(strUrl)")
        let url = NSURL(string: strUrl as String)!
        
        let request = NSMutableURLRequest(url: url as URL)
        let boundary = "Boundary-\(NSUUID().uuidString)"
        
        request.httpMethod = "POST"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let body = NSMutableData();
        
        //print("dictParams = \(dictParams)")
        
        if dictParams != nil {
            for (key, value) in dictParams! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        //let strprint = String(data: body as Data, encoding: String.Encoding.utf8)
        //print("strprintBody First -= \(strprint)");
        var mimetype = "application/octet-stream"
        
        /// Now Image array logic
        //print("arrImages = \(arrImages.count)")
        for i in 0 ..< arrImages.count {
            let dictImage = arrImages[i] as! NSDictionary
            let fileData = dictImage["fileData"] as! NSData
            let thumbData = dictImage["thumbnail"] as! NSData
            let type = dictImage["file_type"] as! String
            if(type == "0") {
                mimetype = "application/octet-stream"
            } else {
                mimetype = "video/mov"
            }
            
            //print("Image fileData = \(fileData.length), thumbData = \(thumbData.length)")
            
            /// FileName
            if(i == 0) {
                if(fileData.length > 0) {
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"file_name0\"; filename=\"\(dictImage["file_name"] as! String)\"\r\n")
                    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                    body.append(fileData as Data)
                    body.appendString(string: "\r\n")
                }
                if(type == "1"){
                    if(thumbData.length > 0) {
                        body.appendString(string: "--\(boundary)\r\n")
                        body.appendString(string: "Content-Disposition: form-data; name=\"thumbnail0\"; filename=\"thumbImage.png\"\r\n")
                        body.appendString(string: "Content-Type: application/octet-stream\r\n\r\n")
                        body.append(thumbData as Data)
                        body.appendString(string: "\r\n")
                    }
                }
                
                
            } else if(i == 1) {
                if(fileData.length > 0) {
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"file_name1\"; filename=\"\(dictImage["file_name"] as! String)\"\r\n")
                    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                    body.append(fileData as Data)
                    body.appendString(string: "\r\n")
                }
                if(type == "1"){
                    if(thumbData.length > 0) {
                        body.appendString(string: "--\(boundary)\r\n")
                        body.appendString(string: "Content-Disposition: form-data; name=\"thumbnail1\"; filename=\"thumbImage.png\"\r\n")
                        body.appendString(string: "Content-Type: application/octet-stream\r\n\r\n")
                        body.append(thumbData as Data)
                        body.appendString(string: "\r\n")
                    }
                }
            } else if(i == 2) {
                if(fileData.length > 0) {
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"file_name2\"; filename=\"\(dictImage["file_name"] as! String)\"\r\n")
                    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                    body.append(fileData as Data)
                    body.appendString(string: "\r\n")
                }
                if(type == "1"){
                    if(thumbData.length > 0) {
                        body.appendString(string: "--\(boundary)\r\n")
                        body.appendString(string: "Content-Disposition: form-data; name=\"thumbnail2\"; filename=\"thumbImage.png\"\r\n")
                        body.appendString(string: "Content-Type:application/octet-stream\r\n\r\n")
                        body.append(thumbData as Data)
                        body.appendString(string: "\r\n")
                    }
                }
            } else if(i == 3) {
                if(fileData.length > 0) {
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"file_name3\"; filename=\"\(dictImage["file_name"] as! String)\"\r\n")
                    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                    body.append(fileData as Data)
                    body.appendString(string: "\r\n")
                }
                if(type == "1"){
                    if(thumbData.length > 0) {
                        body.appendString(string: "--\(boundary)\r\n")
                        body.appendString(string: "Content-Disposition: form-data; name=\"thumbnail3\"; filename=\"thumbImage.png\"\r\n")
                        body.appendString(string: "Content-Type: application/octet-stream\r\n\r\n")
                        body.append(thumbData as Data)
                        body.appendString(string: "\r\n")
                    }
                }
            } else if(i == 4) {
                if(fileData.length > 0) {
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"file_name4\"; filename=\"\(dictImage["file_name"] as! String)\"\r\n")
                    body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                    body.append(fileData as Data)
                    body.appendString(string: "\r\n")
                }
                if(type == "1"){
                    if(thumbData.length > 0) {
                        body.appendString(string: "--\(boundary)\r\n")
                        body.appendString(string: "Content-Disposition: form-data; name=\"thumbnail4\"; filename=\"thumbImage.png\"\r\n")
                        body.appendString(string: "Content-Type: application/octet-stream\r\n\r\n")
                        body.append(thumbData as Data)
                        body.appendString(string: "\r\n")
                    }
                }
            }
            
            
            body.appendString(string: "--\(boundary)--\r\n")
        }
        
        //  _ = String(data: body as Data, encoding: String.Encoding.utf8)
        //print("strprintFileName Array = \(strprintFileName?.characters.count)");
        
        //print("strprintBody -= \(body.length)");
        request.httpBody = body as Data
        return request
    }
    
}
