//
//  SignUpViewController.swift
//  Chayne
//
//  Created by P S 305 on 16/12/16.
//  Copyright © 2016 PS305. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var scrlView1: UIScrollView!
    @IBOutlet weak var scrlView2: UIScrollView!
    
    @IBOutlet weak var view_Second: UIView!
    @IBOutlet weak var view_First: UIView!
    
    @IBOutlet weak var tf_Username: UITextField!
    
    @IBOutlet weak var tf_Password: UITextField!
    
    @IBOutlet weak var tf_ConfirmPassword: UITextField!
    
    @IBOutlet weak var tf_Phone: UITextField!
    @IBOutlet weak var tf_Email: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view_First.alpha = 1.0
        view_Second.alpha = 0.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var btn_SubmitFirst: UIButton!
    @IBOutlet weak var btn_SubmitSecond: UIButton!

    @IBAction func btn_SubmitFirst(_ sender: Any) {
    }
    
    @IBAction func btn_SubmitSecond(_ sender: Any) {
    }
    
    @IBAction func btn_AboutChayne(_ sender: Any) {
    }
    
    @IBAction func btn_BackFirst(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_BackSecond(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
