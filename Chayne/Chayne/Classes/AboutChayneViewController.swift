//
//  AboutChayneViewController.swift
//  Chayne
//
//  Created by P S 305 on 15/12/16.
//  Copyright © 2016 PS305. All rights reserved.
//

import UIKit

class AboutChayneViewController: UIViewController {

    @IBOutlet weak var lbl_AboutChayne: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if(DeviceType.IS_IPHONE_5) {
            lbl_AboutChayne.font = UIFont.boldSystemFont(ofSize: 24.0)
        } else if(DeviceType.IS_IPHONE_6) {
            lbl_AboutChayne.font = UIFont.boldSystemFont(ofSize: 26.0)
        } else if(DeviceType.IS_IPHONE_6P) {
            lbl_AboutChayne.font = UIFont.boldSystemFont(ofSize: 27.0)
        }
        
        
        if(kAppDelegate.isInternetAvailable()) {
            print("Internet is available")
        } else {
            print("Internet is not available")
        }
    }
    @IBAction func btn_GotIt(_ sender: Any) {
        
        if kAppDelegate.viewControllerClass is LoginViewController {
            self.dismiss(animated: true, completion: { 
                })
        } else if (kAppDelegate.viewControllerClass is FirstViewController) {
            self.dismiss(animated: true, completion: {
            })
        } else {
            let vcFirstView = STORYBOARD.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            self.navigationController?.pushViewController(vcFirstView, animated: true)
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
