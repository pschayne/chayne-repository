//
//  ViewController.swift
//  Chayne
//
//  Created by P S 305 on 15/12/16.
//  Copyright © 2016 PS305. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("kAppDelegate.isInternetAvailable() = \(kAppDelegate.isInternetAvailable())")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        kAppDelegate.viewControllerClass = self
        
        self.calculateDayAndNightTime()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func calculateDayAndNightTime() {
        let date = Date()
        // Make Date Formatter
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh a"
        // hh for hour mm for minutes and a will show you AM or PM
        let str = dateFormatter.string(from: date)
        print("\(str)")
        // Sperate str by space i.e. you will get time and AM/PM at index 0 and 1 respectively
        var array = str.components(separatedBy: " ")
        // Now you can check it by 12. If < 12 means Its morning > 12 means its evening or night
        var message = ""
        let personName = "Mr.X"
        let timeInHour = array[0]
        let am_pm = array[1]
        if Int(timeInHour)! < 12 && (am_pm == "AM") {
            message = "Good Morning \(personName)"
        } else if Int(timeInHour)! <= 4 && (am_pm == "PM") {
            message = "Good Afternoon \(personName)"
        } else if Int(timeInHour)! > 4 && (am_pm == "PM") {
            message = "Good Night \(personName)"
        }
        
        print("\(message)")
    }
    

    @IBAction func btn_AboutChayne(_ sender: Any) {
        let vcAbout = STORYBOARD.instantiateViewController(withIdentifier: "AboutChayneViewController") as! AboutChayneViewController
        self.present(vcAbout, animated: true, completion: nil)
    }
    
    @IBAction func btn_Login(_ sender: Any) {
        let vcLogin = STORYBOARD.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(vcLogin, animated: true)
    }
    
    @IBAction func btn_SignUp(_ sender: Any) {
        let vcSignUp = STORYBOARD.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vcSignUp, animated: true)
    }
}

