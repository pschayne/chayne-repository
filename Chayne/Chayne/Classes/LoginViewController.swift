
//
//  LoginViewController.swift
//  Chayne
//
//  Created by P S 305 on 16/12/16.
//  Copyright © 2016 PS305. All rights reserved.
//

import UIKit
import CoreLocation

class LoginViewController: UIViewController, UITextFieldDelegate, BSKeyboardControlsDelegate, CLLocationManagerDelegate {
    var lat: Double = 22.72972464530062
    var long: Double = 75.86343392494253
    var locationManager = CLLocationManager()
    
    let defaults: UserDefaults = UserDefaults.standard
    
    @IBOutlet weak var btn_Enter: UIButton!
    @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var tf_Email: UITextField!
    
    @IBOutlet weak var tf_Password: UITextField!
    
    @IBOutlet weak var btn_AboutChayne: UIButton!
    weak var keyboardControls: BSKeyboardControls?
    override func viewDidLoad() {
        super.viewDidLoad()
        tf_Password.delegate = self
        tf_Email.delegate = self
        // Do any additional setup after loading the view.
        
        let fields = [tf_Email, tf_Password]
        self.keyboardControls = BSKeyboardControls(fields: fields)
        self.keyboardControls?.delegate = self
        btn_Enter.isEnabled = false
        btn_Enter.alpha = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        } else {
            
        }
        
        
        
        
        kAppDelegate.viewControllerClass = self
        tf_Email.text = ""
        tf_Password.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btn_AboutChayne(_ sender: Any) {
        let vcAbout = STORYBOARD.instantiateViewController(withIdentifier: "AboutChayneViewController") as! AboutChayneViewController
        self.present(vcAbout, animated: true, completion: nil)
    }
    
    
    @IBAction func btn_ForgotPassword(_ sender: Any) {
        let vcForgotPassword = STORYBOARD.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(vcForgotPassword, animated: true)
    }
    @IBAction func btn_Enter(_ sender: Any) {
        self.hideKeyBoardView()
        var strMsg = "0"
        
        if(tf_Email.text?.characters.count == 0) {
            strMsg = ValidationMessages.strEnterEmailOrPhone
        } else if((tf_Email.text?.characters.count)! > 0) {
            
            /// For Phone Number
            if(tf_Email.text?.isNumeric())! {
                if((tf_Email.text?.characters.count)! < 10){
                    strMsg = ValidationMessages.strEnterValidPhone
                }
            } else { /// Here I am checking that the string is email or phone no
                //   if ((tf_Email.text?.isValidEmail) != nil){
                if(!Validation.emailCheck(tf_Email!.text)) {
                    strMsg = ValidationMessages.strEnterValidEmail
                }
            }
        } else if (tf_Password.text?.characters.count == 0){
            strMsg = ValidationMessages.strEnterPassword
        } else if ((tf_Password.text?.characters.count)! < 6) {
            strMsg = ValidationMessages.strPasswordLength
        }
        
        
        if(strMsg != "0") { /// Show validation messages
            self.showAlertView(message: strMsg)
        } else {
            //   SVProgressHUD.showWithStatus("Loading", maskType: SVProgressHUDMaskType.Black)
            self.hideKeyBoardView()
            let dictData = NSMutableDictionary()
            
            dictData.setObject(tf_Email.text!, forKey: "email" as NSCopying)
            
            dictData.setObject(tf_Password.text! , forKey: "password" as NSCopying)
            dictData.setObject(kAppDelegate.strDeviceToken, forKey: "device_id" as NSCopying)
            dictData.setObject("1", forKey: "device_type" as NSCopying)
            dictData.setObject(lat, forKey: "latitude" as NSCopying)
            dictData.setObject(long, forKey: "longitude" as NSCopying)
            let arrData = NSMutableArray()
            arrData.add(dictData)
            
            let request = OB_WEBSERVICE.getWebServiceDataWithWebServiceName(webServiceName: WebServiceName.Login, arrParams: arrData, methodType: 1)
            let dataTask = SESSION.dataTask(with: request as URLRequest) {
                (data, response, error) -> Void in
                //print("data = \(data?.length)")
                //print("response = \(response)")
                //print("error = \(error)")
                
                //  let str =  String(data: data!, encoding: NSUTF8StringEncoding)
                //print("str = \(str)")
                //print("array respo = \(try!  NSJSONSerialization.JSONObjectWithData(data!, options: []))")
                if error != nil {
                    self.showAlertView(message: "\(error)")
                    DispatchQueue.main.async(execute: {
                        //   SVProgressHUD.dismiss()
                    })
                    //handle error
                } else {
                    //var jsonObject: NSMutableDictionary = try! JSONSerialization.jsonObject(with: data!, options: []) as! NSMutableDictionary
                    
                    var jsonObject = try! JSONSerialization.jsonObject(with: data!, options: []) as! [String : Any]
                    
                    let products1: AnyObject = jsonObject["message"]! as AnyObject
                    // let error: NSError? = nil
                    //print("Response:\(jsonObject)")
                    if products1.isEqual("success"){
                        DispatchQueue.main.async(execute: {
                            //   SVProgressHUD.dismiss()
                            
                            var dict1 = NSMutableDictionary()
                            dict1 = jsonObject["result"] as! NSDictionary as! NSMutableDictionary//NSMutableDictionary(dictionary: jsonObject["result"])
                            
                            //(jsonObject["result"]! as AnyObject).mutableCopy() as! NSMutableDictionary
                            let archiveUserData = NSKeyedArchiver.archivedData(withRootObject: dict1.mutableCopy())
                            
                            self.defaults.set(true, forKey: "isLogin")
                            self.defaults.set(archiveUserData , forKey: "UserData")
                            self.defaults.synchronize()
                            
                            
                            kAppDelegate.dictUserData =  jsonObject["result"] as! NSDictionary as! NSMutableDictionary //(jsonObject["result"]! as AnyObject).mutableCopy() as! NSMutableDictionary
                                                      //kAppDelegate.vcTabBarViewController.selectedIndex = 0
                            //    kAppDelegate.tabBarControllerClass = DashBoardViewController()
                            print("kAppDelegate.tabBarControllerClass = \(kAppDelegate.tabBarControllerClass)")
                            //   let vcTabBar = self.storyboard?.instantiateViewControllerWithIdentifier("TabBarViewController") as! TabBarViewController
                            //  self.navigationController?.pushViewController(vcTabBar, animated: true)
                            
                        })
                    }else {
                        DispatchQueue.main.async(execute: {
                            // SVProgressHUD.dismiss()
                            self.showAlertView(message: "\(products1)")
                        })
                    }
                }
            }
            DispatchQueue.main.suspend()
            DispatchQueue.main.resume()
            dataTask.resume()
            SESSION.finishTasksAndInvalidate()
        
        }        
    }

    @IBAction func btn_Back(_ sender: Any) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: TextField Delegate Method:
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //        self.keyboardControls.setActiveField(textField)
        if textField.frame.origin.y >= KeyboardHeight {
            scrlView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(textField.frame.origin.y - KeyboardHeight + 10)), animated: true)
            if(textField == tf_Email) {
                scrlView.contentSize = CGSize(width: CGFloat(ScreenSize.SCREEN_WIDTH), height: CGFloat(btn_AboutChayne.frame.origin.y + btn_AboutChayne.frame.size.height + 25))
            } else {
                scrlView.contentSize = CGSize(width: CGFloat(ScreenSize.SCREEN_WIDTH), height: CGFloat(btn_AboutChayne.frame.origin.y + btn_AboutChayne.frame.size.height + KeyboardHeight))
            }
            
        }
        
        self.keyboardControls?.activeField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.hideKeyBoardView()
       
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if((tf_Email.text?.characters.count)! > 0 && (tf_Password.text?.characters.count)! > 0) {
            print("enable")
            btn_Enter.isEnabled = true
            btn_Enter.alpha = 1.0
        } else {
            print("disable")
            btn_Enter.isEnabled = false
            btn_Enter.alpha = 0.5
        }
        
        if(textField == tf_Email) {
            if((tf_Email.text?.isNumeric())!) {
                
                print("Is numeric")
                
                            let currentCharacterCount = tf_Email.text?.characters.count ?? 0
                            if (range.length + range.location > currentCharacterCount){
                                return false
                            }
                            let newLength = currentCharacterCount + string.characters.count - range.length
                            return newLength <= 10
                
            } else {
                print("Is  email")
                return true
            }
        } else {
            return true
        }
        
    }
    
    func keyboardControls(_ keyboardControls: BSKeyboardControls, selectedField field: UIView, in direction: BSKeyboardControlsDirection) {
        print("selectedField = \(field)")
        
        if(field == tf_Email) {
            scrlView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(field.frame.origin.y - KeyboardHeight + 10)), animated: true)
        } else {
            scrlView.setContentOffset(CGPoint(x: CGFloat(0), y: CGFloat(field.frame.origin.y - KeyboardHeight + 10)), animated: true)
        }
        
    }
    
    func keyboardControlsDonePressed(_ keyboardControls: BSKeyboardControls) {
        self.hideKeyBoardView()
    }
    
    private func hideKeyBoardView() {
        tf_Password.resignFirstResponder()
        tf_Email.resignFirstResponder()
        scrlView.contentSize = CGSize(width: CGFloat(ScreenSize.SCREEN_WIDTH), height: CGFloat(btn_AboutChayne.frame.origin.y + btn_AboutChayne.frame.size.height + 5))
    }
    
    func showAlertView(message: String) {
        let alert = UIAlertController(title : "Login Error", message : message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default ,handler:nil))
        self.present(alert, animated: true, completion: nil);
    }
    
    // MARK: Location Delegate Method:
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        lat = locValue.latitude
        long = locValue.longitude
        manager.stopUpdatingLocation()
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
}

extension String {
    
    
    //Validate Email
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    //validate PhoneNumber
    /*
    var isPhoneNumber: Bool {
        
        let charcter  = NSCharacterSet(charactersIn: "+0123456789").inverted
        var filtered:NSString!
        let inputString:NSArray = self.componentsSeparatedByCharactersInSet(charcter)
        filtered = inputString.componentsJoined(by: "") as NSString!
        return  self == filtered
        
    } */
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
        
        // Returns true if the string has at least one character in common with matchCharacters.
        func containsCharactersIn(matchCharacters: String) -> Bool {
            let characterSet = NSCharacterSet(charactersIn: matchCharacters)
            return self.rangeOfCharacter(from: characterSet as CharacterSet) == nil
        }
        
        // Returns true if the string contains only characters found in matchCharacters.
        func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
            let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
            return self.rangeOfCharacter(from: disallowedCharacterSet as CharacterSet) == nil
        }
        
        // Returns true if the string has no characters in common with matchCharacters.
        func doesNotContainCharactersIn(matchCharacters: String) -> Bool {
            let characterSet = NSCharacterSet(charactersIn: matchCharacters)
            return self.rangeOfCharacter(from: characterSet as CharacterSet) == nil
            
        }
        
        // Returns true if the string represents a proper numeric value.
        // This method uses the device's current locale setting to determine
        // which decimal separator it will accept.
        func isNumeric() -> Bool
        {
            let scanner = Scanner(string: self)
            
            // A newly-created scanner has no locale by default.
            // We'll set our scanner's locale to the user's locale
            // so that it recognizes the decimal separator that
            // the user expects (for example, in North America,
            // "." is the decimal separator, while in many parts
            // of Europe, "," is used).
            scanner.locale = NSLocale.current
            
            return scanner.scanDecimal(nil) && scanner.isAtEnd
        }
}
